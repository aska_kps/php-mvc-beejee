<?php

?>

<p><a href="/">Home</a></p>

<?php if(isset($errors) && !empty($errors)): ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach($errors as $error): ?>
            <?= $error . "<br>"; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form method="post">
    <div class="form-group">
        <label for="username">Username</label><br>
        <input type="text" name="username" id="username" class="form-control" value="<?= $user['username']  ?>">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password" value="<?= $user['password'] ?>">
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
</form>

