<p><a href="/">Home</a></p>

<?php if(isset($errors) && !empty($errors)): ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach($errors as $error): ?>
            <?= $error . "<br>"; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form method="post">
    <div class="form-group">
        <label for="username">Username</label><br>
        <input type="text" name="username" id="username" class="form-control" value="<?= $task['username']  ?>">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control" value="<?= $task['email'] ?>">
    </div>
    <div class="form-group">
        <label for="body">Task Body</label>
        <textarea class="form-control" name="body" id="body"><?= $task['body'] ?></textarea>
    </div>
    <?php if ($isAdmin): ?>
        <div class="form-group">
            <label for="completed">Completed</label>
            <input type="checkbox" name="completed" id="completed"  value="1" <?= $task['completed'] ? 'checked' : '' ?>>
        </div>
    <?php endif; ?>
    <button type="submit" class="btn btn-success">Submit</button>
</form>

