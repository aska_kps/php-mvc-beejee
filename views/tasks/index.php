<?php

use app\models\Task;

session_start();

?>

<div class="row justify-content-between align-items-center">
    <div class="col-lg-10">
        <h1>Tasks list</h1>
    </div>
    <div class="col-lg-2">
        <?php if (!$currentUser): ?>
            <a href="/user/login" class="btn btn-success">Login</a>
        <?php else: ?>
            <a href="/user/logout" class="btn btn-success">Logout (<?= $currentUser['username'] ?>)</a>
        <?php endif; ?>
    </div>
</div>


<a href="/tasks/create" class="btn btn-success">Create task</a>


<?php if ($message = $_SESSION['success_message']): ?>
    <div class="alert alert-success">
        <?= $message ?>
        <div class="alert-close">&times;</div>
    </div>
    <?php unset($_SESSION['success_message']); ?>
<?php endif; ?>



<?php if ($message = $_SESSION['danger_message']): ?>
    <div class="alert alert-danger">
        <?= $message ?>
        <div class="alert-close">&times;</div>
    </div>
    <?php unset($_SESSION['danger_message']); ?>
<?php endif; ?>

<?php if (isset($tasks) && $tasks): ?>
    <?php if (isset($sort)): ?>
        <form action="" id="filterForm" method="get">
            <h4>Order by:</h4>
            <div class="form-group">
                <div class="filter__items">
                    <?php foreach ($sort as $item): ?>
                        <div class="filter__item <?= $item == $_GET['sort'] ? 'active' : '' ?> <?= $sortType ?>"
                             data-filter="<?= $item ?>">
                            <?= $item ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <input type="hidden" name="sortType" value="<?= $sortType ?>">
                <input type="hidden" name="sort" value="<?= $_GET['sort'] ?>">
            </div>
        </form>
    <?php endif; ?>

    <?php if($_GET['sort']): ?>
        <a class="btn btn-info" href="/">Reset</a>
    <?php endif; ?>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Body</th>
            <th scope="col">Completed</th>
            <?php if ($isAdmin): ?>
                <th scope="col">Actions</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = $page * $limit; $i < (($page + 1) * $limit); $i++): ?>
            <?php if ($tasks[$i]): ?>
                <tr>
                    <th scope="row"><?= $i + 1 ?></th>
                    <td><?= $tasks[$i]['username'] ?></td>
                    <td><?= $tasks[$i]['email'] ?></td>
                    <td><?= $tasks[$i]['body'] ?></td>
                    <td><?= $tasks[$i]['completed'] ? '<svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check mark completed" viewBox="0 0 16 16">
  <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/>
</svg>' : '<span class="mark" aria-hidden="true">&times;</span>' ?>
                    <?= (Task::getStatusesToSelect()[$tasks[$i]['status']]) ? '<span class="edited">Edited by Admin</span>' : '' ?>

                    </td>
                    <?php if ($isAdmin): ?>
                        <td>
                            <a class="btn btn-primary" href="/tasks/update?id=<?= $tasks[$i]['id'] ?>">Update</a>
                            <a class="btn btn-danger" href="/tasks/delete?id=<?= $tasks[$i]['id'] ?>">Delete</a>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
        <?php endfor; ?>

        </tbody>
    </table>

    <?php if ($page_count > 1): ?>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <?php for ($i = 0; $i < $page_count; $i++): ?>

                    <li class="page-item <?= $page == $i ? 'active' : '' ?>">
                        <a class="page-link"
                           href="?page=<?= $i ?>&sort=<?= $_GET['sort'] ?>&sortType=<?= $_GET['sortType'] ?>"><?= $i + 1 ?></a>
                    </li>
                <?php endfor; ?>
            </ul>
        </nav>
    <?php endif; ?>


<?php else: ?>

<h2 class="empty">There is no Tasks</h2>

<?php endif; ?>

