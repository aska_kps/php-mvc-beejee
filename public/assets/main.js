$('.filter__item').each(function () {
    $(this).on('click', function () {
        $('.filter__item').removeClass('active');
        $(this).addClass('active');
        let filter = $(this).data('filter');
        $('input[name="sort"]').val(filter);
        $('#filterForm').submit();
    });
});

$('.alert-close').on('click', function () {
    $('.alert').hide();
})