<?php

require_once __DIR__.'/../vendor/autoload.php';

use app\Router;
use app\controllers\TaskController;
use app\controllers\UserController;

$router = new Router();

$router->get('/', [TaskController::class, 'index']);
$router->get('/tasks/create', [TaskController::class, 'create']);
$router->get('/tasks/update', [TaskController::class, 'update']);
$router->get('/tasks/delete', [TaskController::class, 'delete']);
$router->post('/tasks/create', [TaskController::class, 'create']);
$router->post('/tasks/update', [TaskController::class, 'update']);
$router->post('/tasks/delete', [TaskController::class, 'delete']);

$router->get('/user/login', [UserController::class, 'login']);
$router->get('/user/signup', [UserController::class, 'signup']);
$router->get('/user/logout', [UserController::class, 'logout']);
$router->post('/user/login', [UserController::class, 'login']);
$router->post('/user/signup', [UserController::class, 'signup']);



$router->resolve();