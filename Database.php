<?php


namespace app;

use app\models\Task;
use app\models\User;
use PDO;


class Database
{
    public \PDO $pdo;
    public static Database $db;

    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;port=3306;dbname=beejee', 'root', 'root',);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        self::$db = $this;
    }

    public function getTasks($sort = null,$sortType='ASC')
    {
        if ($sort) {
            $statement = $this->pdo->prepare("SELECT * FROM tasks ORDER BY $sort $sortType");
            $statement->execute();
        } else {
            $statement = $this->pdo->prepare("SELECT * FROM tasks");
            $statement->execute();
        }


        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createTask(Task $task)
    {
        $statement = $this->pdo->prepare("INSERT INTO tasks (username, email, body, completed, created_at)
                VALUES (:username, :email, :body, :completed, :created_at)");
        $statement->bindValue(':username', $task->username);
        $statement->bindValue(':email', $task->email);
        $statement->bindValue(':body', $task->body);
        $statement->bindValue(':completed', $task->completed);
        $statement->bindValue(':created_at', time());

        $statement->execute();
    }

    public function updateTask(Task $task)
    {
        $statement = $this->pdo->prepare("UPDATE tasks SET username = :username, 
                 email = :email, body = :body, completed = :completed, status = :status WHERE id = :id");
        $statement->bindValue(':username', $task->username);
        $statement->bindValue(':email', $task->email);
        $statement->bindValue(':body', $task->body);
        $statement->bindValue(':completed', $task->completed);
        $statement->bindValue(':status', $task->status);
        $statement->bindValue(':id', $task->id);

        $statement->execute();
    }

    public function deleteTask($id)
    {
        $statement = $this->pdo->prepare('DELETE FROM tasks WHERE id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
    }

    public function getTaskById($id)
    {
        $statement = $this->pdo->prepare('SELECT * FROM tasks WHERE id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function createUser(User $user)
    {
        $statement = $this->pdo->prepare("INSERT INTO users (username, password, role)
                VALUES (:username, :password, :role)");
        $statement->bindValue(':username', $user->username);
        $statement->bindValue(':password', $user->password);
        $statement->bindValue(':role', $user->role);
        $statement->execute();
    }

    public function login(User $user)
    {
        $statement = $this->pdo->prepare('SELECT * FROM users username WHERE username = :username AND password = :password');
        $statement->bindValue(':username', $user->username);
        $statement->bindValue(':password', $user->password);
        $statement->execute();
        if ($auth = $statement->fetch(PDO::FETCH_ASSOC)) {
            return $auth;
        }

        return false;
    }
}