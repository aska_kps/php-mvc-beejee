<?php


namespace app\controllers;


use app\models\Task;
use app\models\User;
use app\Router;

class TaskController
{
    public static function index(Router $router)
    {
        $currentUser = User::getCurrentUser();
        $sortBy = $_GET['sort'] ?? null;
        $sortType = $_GET['sortType'] ?? null;

        $tasks = $router->db->getTasks($sortBy, $sortType);

        $sort = array("username", "email", "body", "completed");

        if ($tasks) {
            $page = $_GET['page'] ?? 0;
            $limit = 3;
            $page_count = ceil(count($tasks) / $limit);
        }

        $sortType = ($_GET['sortType'] == 'desc') ? '' : 'desc';

        $router->renderView('tasks/index', [
            'tasks' => $tasks,
            'sort' => $sort,
            'currentUser' => $currentUser,
            'isAdmin' => User::isAdmin($currentUser),
            'sortType' => $sortType,
            'page' => $page,
            'page_count' => $page_count,
            'limit' => $limit
        ]);
    }

    public static function create(Router $router)
    {
        $errors = [];
        $taskData = [];

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $taskData['username'] = $_POST['username'];
            $taskData['email'] = $_POST['email'];
            $taskData['body'] = $_POST['body'];
            $taskData['completed'] = $_POST['completed'];

            $task = new Task();
            $task->load($taskData);
            $errors = $task->save();

            if (!$errors) {
                session_start();
                $_SESSION['success_message'] = 'Task successfully added';
                header('Location: /');
                exit;
            }
        }

        $router->renderView('tasks/create', [
            'task' => $taskData,
            'errors' => $errors
        ]);


    }

    public static function update(Router $router)
    {
        $currentUser = User::getCurrentUser();
        if (self::checkAccess($currentUser)) {
            $id = $_GET['id'] ?? null;
            $errors = [];

            if (!$id) {
                header('Location: /');
                exit;
            }

            $taskData = $router->db->getTaskById($id);



            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $touched = false;
                if ($taskData['body'] != $_POST['body']) {
                    $touched = true;
                }

                $taskData['username'] = $_POST['username'];
                $taskData['email'] = $_POST['email'];
                $taskData['body'] = $_POST['body'];
                $taskData['completed'] = $_POST['completed'];

                $task = new Task();
                $task->load($taskData, $touched);
                $errors = $task->save();

                if (!$errors) {
                    session_start();
                    $_SESSION['success_message'] = 'Task successfully updated';
                    header('Location: /');
                    exit;
                }
            }

            $router->renderView('tasks/update', [
                'task' => $taskData,
                'errors' => $errors,
                'currentUser' => $currentUser,
                'isAdmin' => User::isAdmin($currentUser)
            ]);
        }
    }

    public static function delete(Router $router)
    {
        $currentUser = User::getCurrentUser();

        if (self::checkAccess($currentUser)) {
            $id = $_GET['id'] ?? null;
            if (!$id) {
                header('Location: /');
                exit;
            }

            $router->db->deleteTask($id);
            header('Location: /');
        }
    }

    public static function checkAccess($currentUser)
    {
        if (!User::isAdmin($currentUser)) {
            session_start();
            $_SESSION['danger_message'] = 'Access denied';
            header('Location: /');
            exit;
        }
        return true;
    }
}