<?php


namespace app\controllers;


use app\models\User;
use app\Router;

class UserController
{
    public static function login(Router $router)
    {
        if (User::isGuest()) {
            $errors = [];
            $userData = [];

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $userData['username'] = $_POST['username'];
                $userData['password'] = $_POST['password'];

                $user = new User();
                $user->load($userData);
                $errors = $user->login();

                if (empty($errors)) {
                    session_start();
                    $_SESSION['success_message'] = 'login success';
                    header('Location: /');
                    exit;
                }
            }

            $router->renderView('users/login', [
                'user' => $userData,
                'errors' => $errors
            ]);
        } else {
            session_start();
            $_SESSION['danger_message'] = 'Already logged in';
            header('Location: /');
        }
    }

    public static function signup(Router $router)
    {

        if (User::isGuest()) {
            $errors = [];
            $userData = [];

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $userData['username'] = $_POST['username'];
                $userData['password'] = $_POST['password'];
                $userData['role'] = $_POST['role'];

                $user = new User();
                $user->load($userData);
                $errors = $user->save();

                if (!$errors) {
                    session_start();
                    $_SESSION['success_message'] = 'user successfully added';
                    header('Location: /');
                    exit;
                }
            }

            $router->renderView('users/signup', [
                'user' => $userData,
                'errors' => $errors
            ]);
        } else {
            session_start();
            $_SESSION['danger_message'] = 'Already logged in';
            header('Location: /');
        }
    }

    public static function logout()
    {
        $currentUser = User::getCurrentUser();
        if ($currentUser) {
            unset($_SESSION['user']);
        }

        header('Location: /');
        exit;
    }


}