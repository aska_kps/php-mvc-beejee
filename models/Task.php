<?php


namespace app\models;


use app\Database;

class Task
{
    const STATUS_TOUCHED = 1;

    public ?int $id = null;
    public ?string $username = null;
    public ?string $email = null;
    public ?string $body = null;
    public ?int $completed = null;
    public ?int $status = null;

    public function load($data, $touched = false)
    {
        $this->id = $data['id'] ?? null;
        $this->username = htmlspecialchars($data['username']);
        $this->email = htmlspecialchars($data['email']);
        $this->body = htmlspecialchars($data['body']);
        $this->completed = $data['completed'] ?? 0;
        $this->status = $touched ? self::STATUS_TOUCHED : null;
    }

    public function save()
    {
        $errors = [];

        if (!$this->username) {
            $errors[] = 'Field "Username" is required';
        }

        if (!$this->email) {
            $errors[] = 'Field "Email" is required';
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'Not a valid email';
        }

        if (!$this->body) {
            $errors[] = 'Field "Body" is required';
        }

        if (!$errors) {
            $db = Database::$db;
            if ($this->id) {
                $db->updateTask($this);
            } else {
                $db->createTask($this);
            }
        }

        return $errors;
    }

    public static function getStatusesToSelect()
    {
        return [
            self::STATUS_TOUCHED => 'Edited by Admin'
        ];
    }
}