<?php


namespace app\models;

use app\Database;

class User
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';


    public ?int $id = null;
    public ?string $username = null;
    public ?string $password = null;
    public ?string $role = null;
    public function load($data)
    {
        $this->id = $data['id'] ?? null;
        $this->username = htmlspecialchars($data['username']);
        $this->password = $data['password'] ? md5(trim(htmlspecialchars($data['password']))) : '';
        $this->role = 'user';
    }

    public function save()
    {
        $errors = $this->validate();

        if (!$errors) {
            $db = Database::$db;
            $db->createUser($this);
        }

        return $errors;
    }

    public function login()
    {
        $errors = $this->validate();
        $db = Database::$db;

        if (!$errors && $user = $db->login($this)) {
            session_start();
            $_SESSION['user'] = $user;
        } elseif (!$errors) {
            $errors[] = 'Invalid username or password';
        }

        return $errors;
    }

    public function validate()
    {
        $errors = [];

        if (!$this->username) {
            $errors[] = 'Field "Username" is required';
        }

        if (!$this->password) {
            $errors[] = 'Field "Password" is required';
        }

        return $errors;
    }



    public static function getCurrentUser()
    {
        session_start();
        if (isset($_SESSION['user']) && $_SESSION['user']) {
            $user = $_SESSION['user'];
            return $user;
        }

        return false;
    }

    public static function isAdmin($user = null)
    {
        if ($user['role'] == self::ROLE_ADMIN) {
            return true;
        }
        return false;
    }

    public static function isGuest()
    {
        $currentUser = self::getCurrentUser();
        if ($currentUser) {
            return false;
        }

        return true;
    }

}